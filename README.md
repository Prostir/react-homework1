# React-homework1

Створити нове React додаток з двома модальними вікнами.

Технічні вимоги:

Створити програму за допомогою create-react-app.
Створити на головній сторінці 2 кнопки з текстом Open first modal і Open second modal.
По кліку на кожну з кнопок має відкриватися відповідне модальне вікно.
Створити компонент Button який повинен мати такі властивості, що передаються з батьківського компонента:

Колір фону (властивість backgroundColor)
Текст (властивість text)
Функція при кліці (властивість onClick)


Створити компонент Modal який повинен мати такі властивості, що передаються з батьківського компонента:

Текст заголовка модального вікна (властивість header)
Чи повинен бути присутнім хрестик для закриття вікна справа вгорі (boolean властивість closeButton, значення true / false)
Основний текст модального вікна, який буде показаний в його центральній частині (властивість text)
Кнопки, які знаходяться в нижній частині модального вікна, передані у вигляді коду в форматі JSX (властивість actions)
При відкритому модальному вікні решта сторінки повинна бути затемнена за допомогою темного напівпрозорого фону.
Модальне вікно повинно закриватися при кліці на затемнену область зовні його контенту.
Стилізувати кнопки і модальні вікна за допомогою SCSS
Кнопки повинні бути різних кольорів
Модальні вікна повинні містити різний текст.
Дизайн модального вікна дається в PSD файлі.
Одне модальне вікно зробити як в прикладі дизайну. Для другого потрібно використовувати інший текст і інші кнопки (виберіть будь-які).
Всі компоненти повинні бути створені у вигляді ES6 класів.