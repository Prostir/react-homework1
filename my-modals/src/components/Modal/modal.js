import React, { Component } from "react";
import "./modal.scss";

class Modal extends Component {
  render() {
    const { closeModal, header, actions, text, backgroundColor } = this.props;
    return (
      <div className="ModalBlock" onClick={closeModal}>
        <div
          className="ModalWindow"
          style={{ backgroundColor: backgroundColor }}
        >
          <div className="ModalHeader">
            <h2>{header}</h2>
            <h2 className="CloseModal" onClick={closeModal}>
              &times;
            </h2>
          </div>
          <div className="ModalBody">
            <p className="ModalText">{text}</p>
            <div className="ModalActions">{actions}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;