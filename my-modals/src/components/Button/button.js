import React, { Component } from "react";
import "./button.scss";

class Button extends Component {
    render() {
      const { text, backgroundColor, onClick, id } = this.props;
      return (
        <button
          className="buttonGeneral"
          style={{ backgroundColor: backgroundColor }}
          onClick={onClick}
          id={id}
        >
          {text}
        </button>
      );
    }
  }
  
  export default Button;