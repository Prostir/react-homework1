
import React, { Component } from "react";
import './App.css';
import Button from "./components/Button/button";
import Modal from "./components/Modal/modal";


class App extends Component {
  state = {
    modalActive: false,
    modalType: null,
  };
  openModal = (type) => {
    this.setState({ modalActive: true, modalType: type });
  };
  closeModal = (e) => {
    if (
      e.target.classList.contains("ModalBlock") ||
      e.target.classList.contains("CloseModal") ||
      e.target.classList.contains("ActionButton")
    )
      this.setState({ modalActive: false, modalType: null });
  };
  render() {
    const { modalActive, modalType } = this.state;
    return (
      <>
        <div className="BlockRoot">
          <Button
            backgroundColor="yellow"
            onClick={() => {
              this.openModal("create");
            }}
            text="Open first modal"
            id="createButton"
          />
          <Button
            backgroundColor="blue"
            onClick={() => {
              this.openModal("delete");
            }}
            text="Open second modal"
            id="deleteButton"
          />
        </div>
        <div>
          {modalActive && modalType === "create" && (
            <Modal
              text="HELLO WORLD!"
              actions={
                <>
                  <button className="ActionButton" onClick={this.closeModal}>
                    OK
                  </button>
                  <button className="ActionButton" onClick={this.closeModal}>
                    CANCEL
                  </button>
                </>
              }
              closeModal={this.closeModal}
              header="Do you want to open this file?"
              backgroundColor="yellow"
            /> 
          )}
          {modalActive && modalType === "delete" && (
            <Modal
              text="Are you sure you want ot delete this?"
              actions={
                <>
                  <button className="ActionButton" onClick={this.closeModal}>
                    OK
                  </button>
                  <button className="ActionButton" onClick={this.closeModal}>
                    CANCEL
                  </button>
                </>
              }
              closeModal={this.closeModal}
              header="Do you want to delete this file?"
              backgroundColor="blue"
            />
          )}
        </div>
      </>
    );
  }
}

export default App;
